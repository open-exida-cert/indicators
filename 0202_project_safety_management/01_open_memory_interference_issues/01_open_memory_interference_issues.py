#!/usr/bin/env python
# coding: utf-8

# In[1]:

import requests
import lxml.etree as etree
import csv
import os

xml_namespace = "{http://cve.mitre.org/cve/downloads/1.0}"
items_count = {}


class Item():
    
    class Ref():
        def __init__(self, ref_element):
            # setting source, url    
            self.__dict__.update(ref_element.attrib)
            self.text = ref_element.text
        
    def _set_text_field(self, element, keyword):
        kw_element = element.find(xml_namespace + keyword)
        if kw_element is not None:
            self.__dict__[keyword] = kw_element.text
        else:
            self.__dict__[keyword] = None
    
    def __init__(self, item_element):
        
        # setting name, seq, type       
        self.__dict__.update(item_element.attrib)
        
        self.year = int(self.name.split("-")[1])
        
        self._set_text_field(item_element, "desc")
        self._set_text_field(item_element, "status")
        
        refs_element = item_element.find(xml_namespace + "refs")
        
        self.refs = [Item.Ref(ref_element) for ref_element in refs_element.findall(xml_namespace + "ref")]
        
        self.url = "https://cve.mitre.org/cgi-bin/cvename.cgi?name=" + self.name


url = "https://cve.mitre.org/data/downloads/allitems.xml"
filename = "allitems.xml"

print('downloading file ...')

print(os.getcwd())

items_xml = requests.get(url)
root = etree.fromstring(items_xml.content)

items = [Item(item_element) for item_element in root.findall(xml_namespace + "item")]
items_count['all_issues'] = len(items)

rh_items = dict()
desc_ctr = 0
ref_ctr = 0

for item in items:
    if item.desc.lower().find("redhat") >= 0 or item.desc.lower().find("red hat") >= 0:
        rh_items[item.name] = item
        desc_ctr += 1        
items_count['rh_desc_issues'] = desc_ctr

for item in items:
    for ref in item.refs:
        if (ref.source is not None and ref.source.lower().find("redhat") >= 0) or (ref.text is not None and ref.text.lower().find("redhat")>= 0):
            rh_items[item.name] = item
            ref_ctr += 1
            break
items_count['rh_ref_issues'] = ref_ctr

outputfile = "mitre_items.csv"
with open(outputfile, "w") as fp:
    for name, item in rh_items.items():
        fp.write(";".join([name, item.url, item.desc])+'\n')

outputfile = "indicators.csv"
with open(outputfile, 'w') as f:
    w = csv.DictWriter(f, items_count.keys())
    w.writeheader()
    w.writerow(items_count)



