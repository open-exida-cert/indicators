import csv

items_count = {}

outputfile = "indicators.csv"
with open(outputfile) as f:
    r = csv.DictReader(f)
    items_count = next(r)

print("Number of all mitre issues:", items_count['all_issues'])
print("Number of issues containing redhat in the description:", items_count['rh_desc_issues'])
print("Number of issues containing redhat in the references:", items_count['rh_ref_issues'])
