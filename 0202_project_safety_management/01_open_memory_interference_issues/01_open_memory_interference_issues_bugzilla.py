#!/usr/bin/env python
#
# This work is licensed under the GNU GPLv2 or later.
# See the COPYING file in the top-level directory.

# query.py: Perform a few varieties of queries




print("this is to print specified fields (without comments)")
for x in range(len(bugs)):
    print("Fetched bug #%s:" % bugs[x].id)
    print("  URL       = %s" % bugs[x].weburl)
    print("  Product   = %s" % bugs[x].product)
    print("  Component = %s" % bugs[x].component)
    print("  Status    = %s" % bugs[x].status)
    print("  Resolution= %s" % bugs[x].resolution)
    print("  Summary   = %s" % bugs[x].summary)
    print("  Severity  = %s" % bugs[x].severity)
    print("  Clones    = %s" % bugs[x].clones)
    print("  Description=%s" % bugs[x].description)
    print("  Keywords  = %s" % bugs[x].keywords)

exit()


print("this is to print specified fields (with comments - slow, but possible to search)")
for x in range(len(bugs)):
    print("Fetched bug #%s:" % bugs[x].id)
    print("  URL   = %s" % bugs[x].weburl)
    print("  Product   = %s" % bugs[x].product)
    print("  Component = %s" % bugs[x].component)
    print("  Status    = %s" % bugs[x].status)
    print("  Resolution= %s" % bugs[x].resolution)
    print("  Summary   = %s" % bugs[x].summary)
    comments = bugs[x].getcomments()
    for y in range(len(comments)):
        print("    Comment   = %s" % comments[y])


#im not sure how to get each bug from bugs, but at least print bugs contains the list.

print("getting one bug")

bug = bzapi.getbug(1651314)
print("Fetched bug #%s:" % bug.id)
print("  Product   = %s" % bug.product)
print("  Component = %s" % bug.component)
print("  Status    = %s" % bug.status)
print("  Resolution= %s" % bug.resolution)
print("  Summary   = %s" % bug.summary)
print("  Summary   = %s" % bug.getcomments())

# Just check dir(bug) for other attributes, or check upstream bugzilla
# Bug.get docs for field names:
# https://bugzilla.readthedocs.io/en/latest/api/core/v1/bug.html#get-bug

# comments must be fetched separately on stock bugzilla. this just returns
# a raw dict with all the info.
comments = bug.getcomments()
print("\nLast comment data:\n%s" % pprint.pformat(comments[-1]))

# getcomments is just a wrapper around bzapi.get_comments(), which can be
# used for bulk comments fetching



# bugzilla.redhat.com, and bugzilla >= 5.0 support queries using the same
# format as is used for 'advanced' search URLs via the Web UI. For example,
# I go to partner-bugzilla.redhat.com -> Search -> Advanced Search, select
#   Classification=Fedora
#   Product=Fedora
#   Component=python-bugzilla
#   Unselect all bug statuses (so, all status values)
#   Under Custom Search
#      Creation date -- is less than or equal to -- 2010-01-01
#
# Run that, copy the URL and bring it here, pass it to url_to_query to
# convert it to a dict(), and query as usual



#    query = bzapi.url_to_query("https://bugzilla.redhat.com/buglist.cgi?classification=Red%20Hat")
#    query["include_fields"] = ["id", "summary"]
#    bugs = bzapi.query(query)
#    print("number of bugs: %s" % len(bugs))

#    print(len(bugs))



# One note about querying... you can get subtley different results if
# you are not logged in. Depending on your bugzilla setup it may not matter,
# but if you are dealing with private bugs, check bzapi.logged_in setting
# to ensure your cached credentials are up to date. See update.py for
# an example usage
