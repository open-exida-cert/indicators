import connectors.bugzilla as bugzilla
#import indicators_load_mitre
#import indicators_load_packages
#import indicators_compute_0205_reported_bugs
import p0808_change_management.p0808_change_management as p0808_change_management



def load():
    bugzilla.bugs_load()



def compute():
    #indicators_compute_0205_reported_bugs.compute(bugs) # jupyter from PL to convert to py
    p0808_change_management.compute()


def main():
    load()
    compute()


if __name__ == '__main__':
  main()
