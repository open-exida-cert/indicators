import time
import bugzilla
import pandas as pd

bugzilla_url = "bugzilla.redhat.com"
bzapi = bugzilla.Bugzilla(bugzilla_url)
pd.set_option('display.max_rows', 1000)


def bugs_load():
    global bugs_df
    products = ["Red Hat Enterprise Linux " + str(i) for i in [4, 5, 6, 7, 8, 9]]

    bugs = []

    for pkg in ["kernel"]:
        try:
            bugs.extend(perform_query(pkg, products, False))
        except:
            print("Timeout occured, dividing %s query into subproducts" % pkg)
            for product in products:
                bugs.extend(perform_query(pkg, product, False))

    mm_keywords = ["corruption", "overflow", "regression", "underflow"]

    bugs_df = pd.DataFrame(data={'id': [bug.id for bug in bugs],
                                 'product': [bug.product for bug in bugs],
                                 'component': [bug.component for bug in bugs],
                                 'status': [bug.status for bug in bugs],
                                 'resolution': [bug.resolution for bug in bugs],
                                 'severity': [bug.severity for bug in bugs],
                                 'summary': [bug.summary for bug in bugs],
                                 'platform': [bug.platform for bug in bugs],
                                 'created': [bug.creation_ts for bug in bugs],
                                 'updated': [bug.last_change_time for bug in bugs],
                                 'is_mm_related': [is_mm_related(bug.description, mm_keywords) for bug in bugs]
                                 })


def perform_query(pkgs_list, products_list, verbose=True):
    query = bzapi.build_query(
        component = pkgs_list,
        product = products_list,
        include_fields = ["id", "summary", "bug_status", "assigned_to", "component", \
                        "product", "resolution", "severity", "clones", "keywords", \
                        "description", "platform", "cf_environment", \
                        "creation_ts", "last_change_time"] )
    t1 = time.time()
    query_bugs = bzapi.query(query)
    t2 = time.time()
    if verbose:
        print("%s %s : processing time: %s, bugs count: %s" % (pkgs_list, products_list, (t2 - t1), len(query_bugs)))
    return query_bugs

def contains_keyword(base_str, keyword):
    return base_str.lower().find(keyword) >= 0

def is_mm_related(base_str, keywords):
    return any([contains_keyword(base_str, keyword) for keyword in keywords])

def get_bugs():        
    return bugs_df
