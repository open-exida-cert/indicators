.. indicators documentation master file, created by
   sphinx-quickstart on Thu Apr 15 13:25:48 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.
   
   This is an Open-Source project managed by exida.

Welcome to indicators's documentation!
======================================

This is an automatic evaluation of indicators for continuous safety certification of software.

This is an Open-Source project managed/owned by exida.

copyright 2021 exida.

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   0201_overall_safety_management/index
   0202_project_safety_management/index
   0605_general/index
   0606_requirements/index
   0607_architecture/index
   0608_design_implementation/index
   0609_unit_verification/index
   0610_integration_verification/index
   0611_testing/index
   0807_configuration_management/index
   p0808_change_management/index
   0809_verification/index
   0810_documentation_management/index
   0811_tools/index


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
