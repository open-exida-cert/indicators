0808 change management
====================================


Kernel memory interference related issues by product
####################################################

The below chart shows the amount of bugs related to memory interference per release. We can see that generally with each release the amount of bugs decreases.


.. jupyter-execute::

    import pandas as pd
    import matplotlib.pyplot as plt

    bugs_indicator_df = pd.read_pickle('bugs_indicator.pkl')
    bugs_indicator_df.plot(kind="bar", title="Kernel memory interference related issues by product", figsize=(15,8))
