import connectors.bugzilla as bugzilla

def compute():
    bugs_df = bugzilla.get_bugs()
    bugs_df[(bugs_df.component == "kernel") & (bugs_df.is_mm_related == True)].groupby(by="product").count().iloc[:, 0].to_pickle('bugs_indicator.pkl')






